package web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Authentification;
import dao.imp.ArticleDaoImp;
import dao.imp.ClientDaoImp;
import dao.interfaces.IClientsDao;
import entities.Client;

@WebServlet("*.do")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();

		if (path.equals("/inscrire.do") && request.getMethod().equals("POST")) {
			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			String adresse = request.getParameter("adresse");
			double codePostal = Double.parseDouble(request.getParameter("codePostal"));
			String ville = request.getParameter("ville");
			String tel = request.getParameter("tel");
			String motPasse = request.getParameter("motPasse");
			String email = request.getParameter("email");

			IClientsDao cltdao = new ClientDaoImp(); // inversion of controle and dependency injection

			/*
			 * valider les donnees chercher ( cltdao.searchClient(email,motpass) ) si il a
			 * deja inscrie si il a une erreur faire un forward vers la meme page mais en
			 * remplissons un champ text par un msg envoyer dans request
			 */
			// verifier si il existe cette email deja dans la base
			int r = cltdao.isExisteClient(email);
			System.out.println(r);
			if (r == 1) {
				request.setAttribute("erreurMsg", "vous ete deja inscrie avec cette email : " + email + " ");
				request.getRequestDispatcher("inscrire.jsp").forward(request, response);
			} else {
				try {

					r = cltdao.insert(new Client(nom, prenom, adresse, codePostal, ville, tel, motPasse, email));

				} catch (SQLException e) {
					e.printStackTrace();
				}
				if (r != 0) {
					HttpSession session = request.getSession();
					Authentification usr = new Authentification(email, motPasse);
					session.setAttribute("usr", usr);
					request.getRequestDispatcher("accueil1.jsp").forward(request, response);
				} else {
					request.getRequestDispatcher("erreurPage.jsp").forward(request, response);
				}
			}
		} else if (path.equals("/identifier.do") && request.getMethod().equals("POST")) {
			String email = request.getParameter("email");
			String motPasse = request.getParameter("motPasse");

			IClientsDao cltdao = new ClientDaoImp();
			Client c = cltdao.searcheClient(email, motPasse);
			if (c == null) {
				request.getRequestDispatcher("identifier.jsp").forward(request, response);
			} else {
				HttpSession session = request.getSession();
				Authentification usr = new Authentification(email, motPasse);
				session.setAttribute("usr", usr);
				request.getRequestDispatcher("accueil1.jsp").forward(request, response);
			}
		} else if (path.equals("/consulter.do")) {
			
			
			request.setAttribute("listArticle", new ArticleDaoImp().getAllArticles());
			request.getRequestDispatcher("catalogue.jsp").forward(request, response);
		} else if (path.equals("/detail.do")) {
			request.setAttribute("listArticle", new ArticleDaoImp().articleByCategorie(1205));
			request.getRequestDispatcher("detail.jsp").forward(request, response);
		} else if (path.equals("/logout.do")) {
			HttpSession session = request.getSession();
			session.removeAttribute("usr");
			session.invalidate();
			request.getRequestDispatcher("accueil.jsp").forward(request, response);
		} else if (path.equals("ajouterAuPanier.do")) {
			int codeArticle=Integer.parseInt(request.getParameter("codeArticle.jsp"));
			
			request.getRequestDispatcher("catalog.jsp").forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
