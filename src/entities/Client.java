package entities;

public class Client {
	private int id;
	private String nom;
	private String prenom;
	private String adresse;
	private double codePostal;
	private String ville;
	private String tel;
	private String motPasse;
	private String Email;
	public Client(String nom, String prenom, String adresse, double codePostal, String ville, String tel,
			String motPasse, String email) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.tel = tel;
		this.motPasse = motPasse;
		Email = email;
	}
	public Client() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public double getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(double codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}
	@Override
	public String toString() {
		return "Client "+this.getNom()+" "+this.getPrenom()+" "+this.getAdresse()+" "+this.getCodePostal()+" "+this.getVille()+" "+this.getTel()+" "+this.getMotPasse()+" "+this.getEmail();
	}
	
}
