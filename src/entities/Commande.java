package entities;

public class Commande {
	private int numCommandes;
	private Client client;
	private String dateCommande;
	public Commande() {
		super();
	}
	public Commande( Client client, String dateCommande) {
		super();
		this.numCommandes = numCommandes;
		this.client = client;
		this.dateCommande = dateCommande;
	}
	public int getNumCommandes() {
		return numCommandes;
	}
	public void setNumCommandes(int numCommandes) {
		this.numCommandes = numCommandes;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public String getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(String dateCommande) {
		this.dateCommande = dateCommande;
	}
	
}
