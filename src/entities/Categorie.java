package entities;

public class Categorie {
	private int refCat;
	private String cat;
	public Categorie(String cat) {
		super();
		this.cat = cat;
	}
	public Categorie() {
		super();
	}
	public int getRefCat() {
		return refCat;
	}
	public void setRefCat(int refCat) {
		this.refCat = refCat;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	
	
}
