package dao.interfaces;

import java.sql.SQLException;
import java.util.List;

import entities.Commande;

public interface ICommandesDao {
	public int insert(Commande cmd)throws SQLException;
	public Commande searchByNumCommande(int NumCommande);
	public List<Commande> searchByClient(int codeClient);
}
