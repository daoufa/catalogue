package dao.interfaces;

import java.sql.SQLException;
import java.util.List;

import entities.Article;

public interface IArticleDao {
	int insert(Article a)throws SQLException;
	public Article searcheArticle(int codeArticle);
	public List<Article> articleByCategorie(int refCat);
	public List<Article> getAllArticles();
}
