package dao.interfaces;

import java.sql.SQLException;
import java.util.List;

import entities.LigneCommande;

public interface ILignesCommandesDao {
	public int insert(LigneCommande lc)throws SQLException;
	public LigneCommande searchByNumCommande(int numCommande);
	public List<LigneCommande> searchByCodeArticle(int codeArticle);
}
