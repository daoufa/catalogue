package dao.interfaces;

import java.util.List;

import entities.Categorie;

public interface ICategoriesDao {
	public Categorie searchByRefCat(int refCat);
	public List<Categorie> searchByCategorie(String categorie);
}
