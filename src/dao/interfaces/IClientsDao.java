package dao.interfaces;

import java.sql.SQLException;

import entities.Client;

public interface IClientsDao {
	int insert(Client c)throws SQLException;
	public Client searcheClient(String email,String motPasse);
	public Client clientById(int id);
	public int isExisteClient(String email);
}
