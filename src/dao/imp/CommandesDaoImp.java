package dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.interfaces.ICommandesDao;
import entities.Commande;

public class CommandesDaoImp implements ICommandesDao{

	@Override
	public int insert(Commande cmd) throws SQLException {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement stm = null;
		int r = 0;
		stm = con.prepareStatement("insert into commandes (codeClient,dateCommande) values(?,?)");

		stm.setInt(1, cmd.getClient().getId());
		stm.setString(2, cmd.getDateCommande());

		r = stm.executeUpdate();
		stm.close();
		return r;
		
	}

	@Override
	public Commande searchByNumCommande(int NumCommande) {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement pr;
		Commande a = null;
		try {
			pr = con.prepareStatement("select * from commandes where numCommandes like ?");
			pr.setInt(1, NumCommande);

			ResultSet rs = pr.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("numCommandes");
				int codeClient = rs.getInt("codeClient");
				String dateCommande = rs.getString("dateCommande");

				a = new Commande(new ClientDaoImp().clientById(codeClient), dateCommande);
				a.setNumCommandes(id);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}

	@Override
	public List<Commande> searchByClient(int codeClient) {
		Connection con = SinglotonConnection.getConnection();
		List<Commande> listCommande=new ArrayList<>();
		PreparedStatement pr;
		Commande a = null;
		try {
			pr = con.prepareStatement("select * from commandes where codeClient like ?");
			pr.setInt(1, codeClient);

			ResultSet rs = pr.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("numCommandes");
				String dateCommande = rs.getString("dateCommande");
				a = new Commande(new ClientDaoImp().clientById(codeClient), dateCommande);
				a.setNumCommandes(id);
				listCommande.add(a);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listCommande;
	}

}
