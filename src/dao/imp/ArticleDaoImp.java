package dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.interfaces.IArticleDao;
import entities.Article;

public class ArticleDaoImp implements IArticleDao {

	@Override
	public int insert(Article a) throws SQLException {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement stm = null;
		int r = 0;
		stm = con.prepareStatement("insert into article (designation,prix,stock,categorie,photo) values(?,?,?,?,?)");

		stm.setString(1, a.getDesignation());
		stm.setInt(2, a.getPrix());
		stm.setInt(3, a.getStock());
		stm.setInt(4, a.getCategorie().getRefCat());
		stm.setString(5, a.getPhoto());

		r = stm.executeUpdate();
		stm.close();
		return r;
	}

	@Override
	public Article searcheArticle(int codeArticle) {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement pr;
		Article a = null;
		try {
			pr = con.prepareStatement("select * from article where codeArticle like ?");
			pr.setInt(1, codeArticle);

			ResultSet rs = pr.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("codeArticle");
				String designation = rs.getString("designation");
				int prix = rs.getInt("prix");
				int stock = rs.getInt("stock");
				int refCat = rs.getInt("Categorie");
				String photo = rs.getString("photo");

				a = new Article(designation, prix, stock, new CategoriesDaoImp().searchByRefCat(refCat), photo);
				a.setCodeArticle(id);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}

	@Override
	public List<Article> articleByCategorie(int refCat) {
		Connection con = SinglotonConnection.getConnection();
		List<Article> listArticle = new ArrayList<Article>();
		try {
			PreparedStatement ps = con.prepareStatement("select * from article where categorie like ?");
			ps.setInt(1, refCat);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("codeArticle");
				String designation = rs.getString("designation");
				int prix = rs.getInt("prix");
				int stock = rs.getInt("stock");
				int categorie = rs.getInt("categorie");
				String photo = rs.getString("photo");

				Article a = new Article(designation, prix, stock, new CategoriesDaoImp().searchByRefCat(categorie),
						photo);
				a.setCodeArticle(id);
				listArticle.add(a);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listArticle;
	}
	
	@Override
	public List<Article> getAllArticles() {
		Connection con = SinglotonConnection.getConnection();
		List<Article> listArticle = new ArrayList<Article>();
		try {
			PreparedStatement ps = con.prepareStatement("select * from article ");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("codeArticle");
				String designation = rs.getString("designation");
				int prix = rs.getInt("prix");
				int stock = rs.getInt("stock");
				int categorie = rs.getInt("categorie");
				String photo = rs.getString("photo");

				Article a = new Article(designation, prix, stock, new CategoriesDaoImp().searchByRefCat(categorie),
						photo);
				a.setCodeArticle(id);
				listArticle.add(a);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listArticle;
	}

}
