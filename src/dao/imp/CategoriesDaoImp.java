package dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.interfaces.ICategoriesDao;
import entities.Categorie;

public class CategoriesDaoImp implements ICategoriesDao{

	@Override
	public Categorie searchByRefCat(int refCat) {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement pr;
		Categorie catg = null;
		try {
			pr = con.prepareStatement("select * from categories where refCat like ?");
			pr.setInt(1, refCat);
			ResultSet rs = pr.executeQuery();
			if (rs.next()) {
				int id=rs.getInt("refCat");
				String cat=rs.getString("cat");
				catg=new Categorie(cat);
				catg.setRefCat(id);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return catg;
	}

	@Override
	public List<Categorie> searchByCategorie(String categorie) {
		Connection con = SinglotonConnection.getConnection();
		List<Categorie> listCategorie=new ArrayList<>();
		PreparedStatement pr;
		Categorie catg = null;
		try {
			pr = con.prepareStatement("select * from categorie where cat like ?");
			pr.setString(1, categorie);
			ResultSet rs = pr.executeQuery();
			while(rs.next()) {
				int id=rs.getInt("refCat");
				String cat=rs.getString("cat");
				catg=new Categorie(cat);
				catg.setRefCat(id);
				listCategorie.add(catg);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listCategorie;
	}
	

}
