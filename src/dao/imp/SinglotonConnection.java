package dao.imp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SinglotonConnection {
	private static Connection connection;
	
	static{
		try {
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/sedo", "root", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() {
		return connection;
	}
}
