package dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.interfaces.IClientsDao;
import entities.Client;

public class ClientDaoImp implements IClientsDao {

	@Override
	public int insert(Client c) throws SQLException {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement stm = null;
		int r = 0;
		stm = con.prepareStatement(
				"insert into client (nom,prenom,adresse,codePostal,ville,tel,motPasse,email) values(?,?,?,?,?,?,?,?)");

		stm.setString(1, c.getNom());
		stm.setString(2, c.getPrenom());
		stm.setString(3, c.getAdresse());
		stm.setDouble(4, c.getCodePostal());
		stm.setString(5, c.getVille());
		stm.setString(6, c.getTel());
		stm.setString(7, c.getMotPasse());
		stm.setString(8, c.getEmail());
		r = stm.executeUpdate();
		stm.close();
		return r;
	}

	@Override
	public Client searcheClient(String email, String motPasse) {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement pr;
		Client c = null;
		try {
			pr = con.prepareStatement("select * from client where email like ? and motPasse like ?");
			pr.setString(1, email);
			pr.setString(2, motPasse);

			ResultSet rs = pr.executeQuery();
			
			if (rs.next()) {
				int id = rs.getInt("id");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String adresse = rs.getString("adresse");
				double codePostal = rs.getDouble("codePostal");
				String ville = rs.getString("ville");
				String tel = rs.getString("tel");
				String mdp = rs.getString("motPasse");
				String email1 =rs.getString("email");
				c=new Client(nom, prenom, adresse, codePostal, ville, tel, mdp, email1);
				c.setId(id);
			}

			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}

	@Override
	public Client clientById(int id) {
		Connection con=SinglotonConnection.getConnection();
		Client clt=null;
		try {
			PreparedStatement ps=con.prepareStatement("select * from client where id like ?");
			ps.setInt(1,id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()) {
				int id1 = rs.getInt("id");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String adresse = rs.getString("adresse");
				double codePostal = rs.getDouble("codePostal");
				String ville = rs.getString("ville");
				String tel = rs.getString("tel");
				String mdp = rs.getString("motPasse");
				String email= rs.getString("email");
				
				clt=new Client(nom, prenom, adresse, codePostal, ville, tel, mdp, email);
				clt.setId(id1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clt;
	}

	@Override
	public int isExisteClient(String email) {
		Client c=searcheClient(email,"%");
		
		if(c==null) return 0;
		else return 1;
	}
	
	

}
