package dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.interfaces.ILignesCommandesDao;
import entities.LigneCommande;

public class LigneCommandesDaoImp implements ILignesCommandesDao{

	@Override
	public int insert(LigneCommande lc) throws SQLException {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement stm = null;
		int r = 0;
		stm = con.prepareStatement("insert into lignesCommandes (codeArticle,qteCde) values(?,?)");

		stm.setInt(1, lc.getArticle().getCodeArticle());
		stm.setInt(2, lc.getQteCde());

		r = stm.executeUpdate();
		stm.close();
		return r;
	}

	@Override
	public LigneCommande searchByNumCommande(int numCommande) {
		Connection con = SinglotonConnection.getConnection();
		PreparedStatement pr;
		LigneCommande a = null;
		try {
			pr = con.prepareStatement("select * from lignesCommandes where numCommande like ?");
			pr.setInt(1, numCommande);

			ResultSet rs = pr.executeQuery();
			if (rs.next()) {
				int id = rs.getInt("numCommande");
				int codeArticle = rs.getInt("codeArticle");
				int qtecde = rs.getInt("qtecde");

				a=new LigneCommande(new CommandesDaoImp().searchByNumCommande(id), new ArticleDaoImp().searcheArticle(codeArticle), qtecde);
				
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}

	@Override
	public List<LigneCommande> searchByCodeArticle(int codeArticle) {
		Connection con = SinglotonConnection.getConnection();
		List<LigneCommande> listLigneCommande=new ArrayList<>();
		PreparedStatement pr;
		LigneCommande a = null;
		try {
			pr = con.prepareStatement("select * from lignesCommandes where codeArticle like ?");
			pr.setInt(1, codeArticle);

			ResultSet rs = pr.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("numCommande");
				int codeArticle1 = rs.getInt("codeArticle");
				int qtecde = rs.getInt("qtecde");
				a=new LigneCommande(new CommandesDaoImp().searchByNumCommande(id), new ArticleDaoImp().searcheArticle(codeArticle1), qtecde);
				listLigneCommande.add(a);
			}
			pr.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listLigneCommande;
	}

}
