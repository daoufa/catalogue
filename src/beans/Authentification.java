package beans;

import java.io.Serializable;

public class Authentification implements Serializable {
	private String email;
	private String motpasse;
	public Authentification() {
		super();
	}
	public Authentification(String email, String motpasse) {
		super();
		this.email = email;
		this.motpasse = motpasse;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMotpasse() {
		return motpasse;
	}
	public void setMotpasse(String motpasse) {
		this.motpasse = motpasse;
	}
	
}
