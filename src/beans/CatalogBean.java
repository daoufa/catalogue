package beans;

import java.io.Serializable;

public class CatalogBean implements Serializable{
	private int reference;
	private String titre;
	private String auteur;
	private String photo;
	private int prix;
	public CatalogBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CatalogBean(int reference, String titre, String auteur, String photo, int prix) {
		super();
		this.reference = reference;
		this.titre = titre;
		this.auteur = auteur;
		this.photo = photo;
		this.prix = prix;
	}
	public int getReference() {
		return reference;
	}
	public void setReference(int reference) {
		this.reference = reference;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	
}
