<!DOCTYPE html>
<html>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<head>
<meta charset="ISO-8859-1">
<title>Accueil</title>

<style type="text/css">
body {
	background-color: #99ffff;
}

h1 {
	color: #cc0099;
	font-size: 24pt;
	text-align: center;
	margin-left: 100px;
	margin-right: 100px;
	font-family: cursive, bold, italic;
}

a {
	color: #ff0099;
	font-size: 18pt;
	margin-left: 100px;
}
#header{
padding:15px;
margin:5px;
font-size:16pt;
}
</style>
</head>
<body>
	<c:if test="${not empty usr}">
		<div class="navbar navbar-inverse">
			<a href="accueil.jsp" id="header">Home</a> 
			<a href="logout.do" id="header">Logout</a>
		</div>
			<h1>Bonjour ${usr.email }</h1>
			<a href="consulter.do">Consulter le catalogue</a> <br /> <a
				href="suivre">Suivre vos commandes</a> <br /> <a href="visualiser">Visualiser
				votre panier</a>
	</c:if>
	<c:if test="${empty usr }">
		<p>vous devez authentifier d'abord</p>
		<a href="accueil.jsp">revenir a la page d'accueil </a>
	</c:if>
</body>
</html>