<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<head>
<meta charset="ISO-8859-1">
<title>identifier</title>

<style type="text/css">
body {
	background-color: #99ffff;
}

h1 {
	color: #cc0099;
	font-size: 30pt;
	text-align: center;
	margin-left: 100px;
	margin-right: 100px;
	font-family: cursive, bold, italic;
}

label {
	color: purple;
	font-size: 16pt;
}

tr {
	text-align: center;
}
#header{
padding:15px;
margin:5px;
font-size:16pt;
}
</style>

</head>
<body>
<div class="navbar navbar-inverse">
		<a href="accueil.jsp" id="header">Home</a>
</div>
	<h1>Identifiez-vous</h1>
	<form action="identifier.do" method="post">
		<table>
			<tr>
				<th><label>E-mail</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="email"></th>
			</tr>
			<tr>
				<th><label>Mot de passe</label></th>
				<th><label>:</label></th>
				<th><input type="password" name="motPasse"></th>
				<th id="btn"><input type="submit" name="ok" value="OK"></th>
			</tr>
		</table>
	</form>
</body>
</html>