<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" /> -->
<head>
<meta charset="ISO-8859-1">
<title>s'inscrire</title>

<style type="text/css">
h1 {
	color: #cc0099;
	font-size: 30pt;
	text-align: center;
	margin-left: 100px;
	margin-right: 100px;
	font-family: cursive, bold, italic;
}

label {
	color: purple;
	font-size: 16pt;
}

body {
	background-color: #99ffff;
}
#header{
padding:15px;
margin:5px;
font-size:16pt;
}
</style>
</head>
<body>
<div class="navbar navbar-inverse">
		<a href="accueil.jsp" id="header">Home</a>
</div>
	<h1>Inscrivez-vous</h1>
	<form action="inscrire.do" method="post">
		<table>
			<tr>
				<th><label>Nom</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="nom"></th>
			</tr>
			<tr>
				<th><label>Prenom</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="prenom"></th>
			</tr>
			<tr>
				<th><label>Adresse</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="adresse" required="required"></th>
				<th><label>code Postale</label><input type="text" name="codePostal" required="required"></th>
				<th><label>ville</label><input type="text" name="ville" required="required"></th>
			</tr>
			<tr>
				<th><label>TEL</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="tel" required="required"></th>
			</tr>
			<tr>
				<th><label>Mot de passe</label></th>
				<th><label>:</label></th>
				<th><input type="password" name="motPasse" required="required"></th>
			</tr>
			<tr>
				<th><label>E-mail</label></th>
				<th><label>:</label></th>
				<th><input type="text" name="email" required="required" ></th>
				<th><input type="submit" value="OK"></th>
			</tr>
		</table>
	</form>
	<h1><c:out value="${erreurMsg}" /></h1>
</body>
</html>