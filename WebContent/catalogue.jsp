<%@page import="entities.Article"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<head>
<meta charset="ISO-8859-1">
<title>Catalogue</title>

<style type="text/css">
body {
	background-color: #99ffff;
}

h1 {
	color: #cc0099;
	font-size: 30pt;
	text-align: center;
	margin-left: 100px;
	margin-right: 100px;
	font-family: cursive, bold, italic;
}

label {
	font-color: purple;
	font-size: 16pt;
}
</style>
</head>
<body>
	<h1>Catalogue</h1>




	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<form>
					Choisissez le genre : <select name="genre">
						<option>Jazz</option>
					</select>
				</form>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<tr>
						<th>reference</th>
						<th>designation</th>
						<th>prix</th>
						<th>stock</th>
						<th>photo</th>
					</tr>
					<c:forEach items="${listArticle}" var="l">
						<tr>
							<td><a href="detail.jsp?codeArticle=${l.codeArticle }">${l.codeArticle }</a></td>
							<td>${l.designation }</td>
							<td>${l.prix }</td>
							<td>${l.stock }</td>
							<td>${l.photo }</td>
							<td><a href="ajouterAuPanier.do?codeArticle=${l.codeArticle }">ajouter
									au panier</a>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</div>


</body>
</html>